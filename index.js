function calSalary(){
    event.preventDefault();
    var salaryPerDay = document.getElementById("inputNum1").value*1
    var dayWork = document.getElementById("inputNum2").value*1
    var totalSalary = salaryPerDay*dayWork;
    document.getElementById("txtSalary").innerHTML=`Tiền lương của bạn là ${totalSalary}`
}
function calAverage(){
    event.preventDefault()
    var num1 = document.getElementById("num1").value*1
    var num2 = document.getElementById("num2").value*1
    var num3 = document.getElementById("num3").value*1
    var num4 = document.getElementById("num4").value*1
    var num5 = document.getElementById("num5").value*1
    var average = (num1+num2+num3+num4+num5)/5;
    document.getElementById("txtTB").innerHTML=`Trung bình cộng = ${average}`
}
function calCurrency(){
    event.preventDefault()
    const oneVndToUsd = 23500*1;
    var usd = document.getElementById("usd").value*1;
    var usdToVnd = usd*oneVndToUsd;
    document.getElementById("txtCurrency").innerHTML=`Đổi ${usd}$ sang VND => ${usdToVnd.toLocaleString()} VND`;
}
function calCviDtich(){
    event.preventDefault()
    var width = document.getElementById("width").value*1
    var height = document.getElementById("height").value*1
    var chuVi = (width+height)*2
    var dienTich = width*height
    document.getElementById("txtCal").innerHTML=`Kết quả <br> Chu vi : ${chuVi} <br> Diện tích : ${dienTich} `
}
function calTwoDigits(){
    event.preventDefault()
    var number = document.getElementById("number").value*1
    var dozens = Math.floor(number/10)
    var units = number%10
    var sumDigits = dozens + units
    document.getElementById("txtSum").innerHTML=` Tổng hai ký số của ${number} là : ${sumDigits} `
}